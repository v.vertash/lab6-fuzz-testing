import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


function multiplier(type) {
    return type === "Standard" ? 0.05 :
            type === "Premium" ? 0.1 :
            type === "Diamond" ? 0.2 : 0;
}


function bonus(bonusAmount) {
    return bonusAmount < 10000 ? 1 :
            bonusAmount < 50000 ? 1.5 :
            bonusAmount < 100000 ? 2 : 2.5;
}


describe('Bonus system tests', () => {

    test('below value standart test ',  (done) => {
        let type = "Standard";
        let bonusAmount = 0;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('standart low value equal test',  (done) => {
        let type = "Standard";
        let bonusAmount = 10000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('standart middle value test',  (done) => {
        let type = "Standard";
        let bonusAmount = 50000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('standart upper value test',  (done) => {
        let type = "Standard";
        let bonusAmount = 100000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });
	
	test('standart above upper value test',  (done) => {
        let type = "Standard";
        let bonusAmount = 1000000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('premium below low value test',  (done) => {
        let type = "Premium";
        let bonusAmount = 0;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('premium low value test ',  (done) => {
        let type = "Premium";
        let bonusAmount = 10000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('premium midle value test ',  (done) => {
        let type = "Premium";
        let bonusAmount = 50000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('premium uper value test ',  (done) => {
        let type = "Premium";
        let bonusAmount = 100000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });
	
	test('premium above upper value test ',  (done) => {
        let type = "Premium";
        let bonusAmount = 150000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('diamond below low value test',  (done) => {
        let type = "Diamond";
        let bonusAmount = 2000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('diamond low value test',  (done) => {
        let type = "Diamond";
        let bonusAmount = 10000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('diamond middle value test',  (done) => {
        let type = "Diamond";
        let bonusAmount = 50000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('diamond upper value test',  (done) => {
        let type = "Diamond";
        let bonusAmount = 100000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });
	
	test('diamond above upper value test',  (done) => {
        let type = "Diamond";
        let bonusAmount = 200000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('nonsense value test 1',  (done) => {
        let type = "nonsense";
        let bonusAmount = 2000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('nonsense value test 2',  (done) => {
        let type = "nonsense";
        let bonusAmount = 10000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('nonsense value test 3',  (done) => {
        let type = "nonsense";
        let bonusAmount = 50000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

    test('nonsense value test 4',  (done) => {
        let type = "nonsense";
        let bonusAmount = 100000;
        assert.equal(calculateBonuses(type, bonusAmount), multiplier(type) * bonus(bonusAmount));
        done();
    });

});
